# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-28 15:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('smart_rm', '0007_auto_20170628_1512'),
    ]

    operations = [
        migrations.AddField(
            model_name='taskmodel',
            name='action',
            field=models.CharField(default=None, max_length=1000),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='taskmodel',
            name='files',
            field=models.ManyToManyField(default=None, to='smart_rm.FileModel'),
        ),
    ]
