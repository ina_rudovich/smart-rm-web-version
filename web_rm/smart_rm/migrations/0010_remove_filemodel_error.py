# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-28 18:14
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('smart_rm', '0009_auto_20170628_1736'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='filemodel',
            name='error',
        ),
    ]
