# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-23 14:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('smart_rm', '0002_auto_20170620_1829'),
    ]

    operations = [
        migrations.CreateModel(
            name='FileModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_path', models.CharField(max_length=1000)),
            ],
        ),
        migrations.AddField(
            model_name='bucketmodel',
            name='max_size',
            field=models.CharField(default=1000000, max_length=100),
        ),
        migrations.AddField(
            model_name='bucketmodel',
            name='size_files_storage',
            field=models.CharField(default=10000, max_length=100),
        ),
        migrations.AddField(
            model_name='bucketmodel',
            name='time_files_storage',
            field=models.CharField(default=10, max_length=100),
        ),
        migrations.AddField(
            model_name='filemodel',
            name='bucket_name',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='smart_rm.BucketModel', verbose_name='bucket'),
        ),
    ]
