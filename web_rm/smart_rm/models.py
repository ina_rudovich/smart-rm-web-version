# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.db import models


class BucketModel(models.Model):
    name = models.CharField(max_length=100, unique=True)
    bucket_path = models.CharField(max_length=1000, unique=True)
    files_path = models.CharField(max_length=1000, unique=True)
    info_path = models.CharField(max_length=1000, unique=True)
    history_path = models.CharField(max_length=1000, unique=True)
    max_size = models.PositiveIntegerField(default=1000000)
    time_files_storage = models.PositiveIntegerField(default=10)
    size_files_storage = models.PositiveIntegerField(default=10000)


class TaskModel(models.Model):
    status = models.CharField(max_length=1000)
    action = models.CharField(max_length=1000)
    bucket = models.ForeignKey(BucketModel)


class FileModel(models.Model):
    task=models.ForeignKey(TaskModel, default=None)
    file_path = models.CharField(max_length=1000)
    error = models.TextField(null=True)
