from django.conf.urls import url
from django.contrib import admin

from smart_rm import views

urlpatterns = [
    url(r'^view/(?P<path>.+)?', views.select_files_for_removing_view, name='view_files'),
    url(r'^remove/', views.remove_files, name='remove_files'),
    url(r'^add_bucket/', views.AddBucketView.as_view(), name='add_bucket'),
    url(r'^buckets/', views.view_buckets, name='view_buckets'),
    url(r'^del_bucket/(?P<pk>\d+)', views.BucketDelete.as_view(), name="del_bucket"),
    url(r'^bucket_files/(?P<pk>\d+)',views.view_bucket_contents, name="bucket_files" ),
    url(r'^restore_files/', views.restore_files, name="restore_files"),
    url(r'^clean_bucket/(?P<pk>\d+)',views.clean_bucket, name="clean_bucket"),
    url(r'^tasks/(?P<pk>\d+)', views.tasks_view, name="tasks"),
    url(r'^task_result/(?P<pk>\d+)', views.task_result_view, name="task_result" )
]
