from django import forms
from smart_rm import models

class AddBucketForm(forms.ModelForm):
    class Meta:
        model = models.BucketModel
        fields = ["name", "bucket_path", "max_size","time_files_storage","size_files_storage"]

