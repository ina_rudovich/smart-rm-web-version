# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
import os

from django.urls import reverse_lazy
from django.views.generic import FormView
from django.views.generic.edit import DeleteView

from smart.bucket import Bucket
from smart.remover import Remover
from smart.log import create_logger
from smart import smart_rm, smart_trash
from smart.file_utils import regular_expression_search

from models import BucketModel, TaskModel, FileModel

# Create your views here.
from smart_rm.forms import AddBucketForm


def select_files_for_removing_view(request, path):
    if path is None:
        path = os.path.expanduser("~")
    files =[]
    for file in os.listdir(path):
        file_path =  os.path.join(path,file)
        if os.path.islink(file_path):
            file_type = "link"
        elif os.path.isfile(file_path):
            file_type = "file"
        elif os.path.isdir(file_path):
            file_type = "directory"
        else:
            file_type = "unknown"

        read_ok = os.access(file_path, os.R_OK)

        files.append({"name": file, "path": file_path, "type": file_type,"read_ok": read_ok})

    directories = dir_paths_from_current_to_root(path)

    buckets = BucketModel.objects.values()

    return render(
        request=request,
        template_name='smart_rm/smart_remove.html',
        context={
            'files': files,
            'directories': directories,
            'buckets': buckets,
            'current_dir': path
        }
    )


def dir_paths_from_current_to_root(path):
    directories = []
    current_dir = os.path.abspath(path)
    dir_name =  os.path.basename(path)
    while len(dir_name):
        directories.append({"name": dir_name,"path": current_dir})
        current_dir = os.path.dirname(current_dir)
        dir_name = os.path.basename(current_dir)
    else:
        directories.append({"name": "/","path": "/"})
    directories.reverse()
    return directories


def remove_files(request):
    files = request.POST.getlist("added[]")
    bucket_id = request.POST.getlist("bucket_id")[0]
    force_flag = bool(request.POST.getlist("force"))
    recursive_flag = bool(request.POST.getlist("recursive"))
    directory_flag = bool(request.POST.getlist("dir"))
    regex = request.POST.getlist("regex")[0]
    regex_files = []
    if regex:
        for file in files:
            regex_files.extend(regular_expression_search(
                directory=file,
                regular_expression=regex))

        files = regex_files

    bucket = BucketModel.objects.get(id=bucket_id)
    remover = Remover(
        # dry_run=arguments.dry_run,
        # interactive=arguments.interactive,
        force=force_flag,
        dir_flag=directory_flag,
        recursive_flag=recursive_flag,
        bucket_path=bucket.bucket_path,
        bucket_info_path=bucket.info_path,
        bucket_files_path=bucket.files_path,
        history_path=bucket.history_path,
        max_bucket_size=bucket.max_size,
    )

    logger  = create_logger(
        history_path=bucket.history_path,
        dry_run=False,
    )

    exceptions = smart_rm.remove_files(
        remover=remover,
        files=files,
        logger=logger
    )[0]
    del(logger)


    task = save_task(
        action="Remove",
        bucket=bucket,
        files=files,
        exceptions=exceptions
    )

    return render(
        request=request,
        template_name='smart_rm/result_form.html',
        context={
            "task": task,
            "errors_count": len(exceptions),
            "files": FileModel.objects.filter(task_id=task.id)
        }
    )


class AddBucketView(FormView):
    form_class = AddBucketForm

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)

        return render(
            request=request,
            template_name='smart_rm/add_bucket_form.html',
            context={'form': form}
        )

    def post(self, request, *args,**kwargs):
        form = self.form_class(data=request.POST)

        if not form.is_valid():
                return render(
                request=request,
                template_name='smart_rm/add_bucket_form.html',
                context={'form': form}
            )

        if os.path.isabs(form.cleaned_data["bucket_path"]):
            bucket_path = form.cleaned_data["bucket_path"]
        else:
            bucket_path = os.path.join(os.path.expanduser("~"), form.cleaned_data["bucket_path"])


        bucket = BucketModel(
            name=form.cleaned_data["name"],
            bucket_path=bucket_path,
            files_path=os.path.join(bucket_path,"files"),
            info_path=os.path.join(bucket_path,"info"),
            history_path=os.path.join(bucket_path,"history_path"),
            max_size=form.cleaned_data["max_size"],
            time_files_storage=form.cleaned_data["time_files_storage"],
            size_files_storage=form.cleaned_data["size_files_storage"],
        )
        bucket.save()

        task = save_task(
            action="Add bucket",
            bucket=bucket,
        )

        Bucket(
            bucket_path=bucket.bucket_path,
            bucket_files_path=bucket.files_path,
            bucket_info_path =bucket.info_path,
        ).prepare()

        return HttpResponseRedirect(reverse_lazy('smart_rm:view_buckets'))


def view_buckets(request):

    buckets = BucketModel.objects.values()
    return render(
        request=request,
        template_name='smart_rm/smart_bucket.html',
        context={'buckets':buckets}
    )

def view_bucket_contents(request, pk):
    bucket = BucketModel.objects.get(id=pk)
    contents = Bucket(
        bucket_path=bucket.bucket_path,
        bucket_info_path=bucket.info_path,
        bucket_files_path=bucket.files_path
    ).get_contents()
    return render(
        request=request,
        template_name='smart_rm/bucket_files.html',
        context={
            'bucket_contents': contents,
            'bucket_id': pk,
        }
    )


def restore_files(request):
    files=request.POST.getlist("added")
    bucket_id = request.POST.getlist("bucket_id")[0]
    bucket_model = BucketModel.objects.get(id=bucket_id)
    bucket = Bucket(
        bucket_path=bucket_model.bucket_path,
        bucket_info_path=bucket_model.info_path,
        bucket_files_path=bucket_model.files_path,
        history_path=bucket_model.history_path,
    )
    logger = create_logger(
        history_path=bucket_model.history_path,
        dry_run=False,
    )

    exceptions = smart_trash.restore_files(
        logger=logger,
        bucket=bucket,
        files=files
    )[0]

    task = save_task(
        action="Restore",
        bucket=bucket_model,
        files=files,
        exceptions=exceptions
    )

    return render(
        request=request,
        template_name='smart_rm/result_form.html',
        context={
            "task": task,
            "errors_count": len(exceptions),
            "files": FileModel.objects.filter(task_id=task.id)
        }
    )


class BucketDelete(DeleteView):
    model = BucketModel
    # task = save_task(
    #     bucket=model,
    #     action="Delete bucket"
    # )
    success_url = reverse_lazy('smart_rm:view_buckets')



def clean_bucket(request,pk):
    time_cleaning_flag = bool(request.POST.getlist("time_flag"))
    size_cleaning_flag = bool(request.POST.getlist("size_flag"))
    time = request.POST.get("time")
    size = request.POST.get("size")
    bucket_model = BucketModel.objects.get(id=pk)

    if time_cleaning_flag and len(time):
        time = int(time)
    else:
        time = bucket_model.time_files_storage

    if size_cleaning_flag and len(size):
        size = int(size)
    else:
        size = bucket_model.size_files_storage

    bucket = Bucket(
        bucket_path=bucket_model.bucket_path,
        bucket_files_path=bucket_model.files_path,
        bucket_info_path=bucket_model.info_path,
        size_files_storage=size,
        time_files_storage=time
    )

    actions=[]
    if time_cleaning_flag:
        bucket.clean_by_time()
        actions.append('time')

    if size_cleaning_flag:
        bucket.clean_by_size()
        actions.append('size')


    if not(size_cleaning_flag or time_cleaning_flag):
        bucket.clean()

    if len(actions) == 0:
        action_message = "Cleaned bucket"
    else:
        action_message = "Cleaned bucket (%s)" % ', '.join(actions)

    task = save_task(
        action=action_message,
        bucket=bucket_model,
    )

    return render(
        request=request,
        template_name='smart_rm/result_form.html',
        context={
            "task": task,
            "errors_count": 0,
            "files": FileModel.objects.filter(task_id=task.id)
        }
    )


def tasks_view(request, pk):
    bucket = BucketModel.objects.get(id=pk)
    tasks = TaskModel.objects.filter(bucket=bucket)

    return render(
        request=request,
        template_name="smart_rm/bucket_tasks.html",
        context={
            "tasks": tasks
        }
    )


def task_result_view(request, pk):

    task = TaskModel.objects.get(id=pk)
    files = FileModel.objects.filter(task_id=task.id)

    errors_count = 0
    for file in files:
        if file.error:
            errors_count+=1

    return render(
        request=request,
        template_name='smart_rm/result_form.html',
        context={
            "task": task,
            "errors_count": errors_count,
            "files": files
        }
    )


def save_task(
        action,
        bucket,
        files=None,
        exceptions=None,
):
    if exceptions:
        status="Errors"
    else:
        status="OK"

    task = TaskModel(
        action=action,
        status=status,
        bucket=bucket)

    task.save()

    if action == "Remove" or action == "Restore":
        unprocessed_files = set([exception["file"] for exception in exceptions])
        processed_files = set(files).difference(unprocessed_files)

        for file in processed_files:
            file_model = FileModel(file_path=file,task=task,error=None)
            file_model.save()

        for exception in exceptions:
            file_model = FileModel(
                file_path=exception["file"],
                error=exception["strerror"],
                task=task
            )
            file_model.save()

    return task
